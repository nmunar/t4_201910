package model.util;

/**
 * Metodos impelemtados de Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne
 */
public class Sort {

/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) {

		// DONE implementar el algoritmo ShellSort
		int n = datos.length;
		// 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
		int h = 1;
		while (h < n/3) h = 3*h + 1; 

		while (h >= 1) {
			// h-sort the array
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h) {
					exchange(datos, j, j-h);
				}
			}
			h /= 3;
		}
	}

	//----------------------------------------------------------------------------------------------------------

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	private static void merge( Comparable[ ] datos, Comparable[ ] aux, int lo,int mid ,int hi ) 
	{
		assert isSorted(datos, lo, mid);
		assert isSorted(datos, mid+1, hi);

		// copy to aux[]
		for (int k = lo; k <= hi; k++) 
		{
			aux[k] = datos[k]; 
		}

		// merge back to a[]
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if      (i > mid)              datos[k] = aux[j++];
			else if (j > hi)               datos[k] = aux[i++];
			else if (less(aux[j], aux[i])) datos[k] = aux[j++];
			else                           datos[k] = aux[i++];
		}

		// postcondition: a[lo .. hi] is sorted
		assert isSorted(datos, lo, hi);
	}


	public static void ordenarMergeSort(Comparable[] a, Comparable[] aux, int lo, int hi) 
	{
		if (hi <= lo) return;
		int mid = lo + (hi - lo) / 2;
		ordenarMergeSort(a, aux, lo, mid);
		ordenarMergeSort(a, aux, mid + 1, hi);
		merge(a, aux, lo, mid, hi);
	}
	private static void odenarMergeSort(Comparable[] a) 
	{
		Comparable[] aux = new Comparable[a.length];
		ordenarMergeSort(a, aux, 0, a.length-1);
		assert isSorted(a);
	}

	private static boolean isSorted(Comparable[] a) {
		return isSorted(a, 0, a.length - 1);
	}

	private static boolean isSorted(Comparable[] a, int lo, int hi) {
		for (int i = lo + 1; i <= hi; i++)
			if (less(a[i], a[i-1])) return false;
		return true;
	}

	//----------------------------------------------------------------------------------------------------------
	private static int partition(Comparable[] a, int lo, int hi) 
	{    
		int i = lo, j = hi+1;    while (true)    
		{       
			while (less(a[++i], a[lo]))          
				if (i == hi) break;       
			while (less(a[lo], a[--j]))          
				if (j == lo) break;       
			if (i >= j) break;       
			exchange(a, i, j);    
		}    exchange(a, lo, j);    
		return j; 
	} 
	
	public static void ordenarQuickSort(Comparable[] a)    {       
		StdRandom.shuffle(a);       
		quickSort(a, 0, a.length - 1);    
		}

	
	
	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void quickSort(Comparable[] a, int lo, int hi)    
	{       
		if (hi <= lo) return;       
		int j = partition(a, lo, hi);       
		quickSort(a, lo, j-1);       
		quickSort(a, j+1, hi);   

	}



	//----------------------------------------------------------------------------------------------------------

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		// DONE implementar
		return (v.compareTo(w) < 0);
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// DONE implementar
		Comparable swap = datos[i];
		datos[i] = datos[j];
		datos[j] = swap;
	}

}
