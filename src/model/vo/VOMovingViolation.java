package model.vo;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	// DONE Definir los atributos de una infraccion
	private int objectId;

	private String location;

	private String ticketIssueDate;

	private int totalPaid;

	private String accidentIndicator;

	private String violationDescription;


	/**
	 * Metodo constructor
	 */
	public VOMovingViolation( String pObjectId, String pLocation, String pTicketIssueDate, String pTotalPaid, String pAccidentIndicator, String pViolationDescription )
	{
		// DONE Implementar
		objectId = Integer.parseInt(pObjectId);
		location = pLocation;
		ticketIssueDate = pTicketIssueDate;
		totalPaid = Integer.parseInt(pTotalPaid);
		accidentIndicator = pAccidentIndicator;
		violationDescription = pViolationDescription;

	}	

	/**
	 * @return id - Identificador unico de la infraccion
	 */
	public int objectId() {
		return objectId;
	}	


	/**
	 * @return location - Direccion en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente paga el que recibio la infraccion en USD.
	 */
	public int getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String  getViolationDescription() {
		return violationDescription;
	}

	@Override
	public int compareTo(VOMovingViolation o) {
		int deter = 90;
		if(o.getTicketIssueDate().equals(this.getTicketIssueDate()))
		{
			if(o.objectId()==this.objectId)
			{
				deter = 0;
			}else if(o.objectId()<this.objectId)
			{
				deter = 1;
			}else if(o.objectId()>this.objectId)
			{
				deter = -1;
			}

		}else if(o.getTicketIssueDate().compareTo(this.getTicketIssueDate())<0)
		{
			deter = 1;
		}else if(o.getTicketIssueDate().compareTo(this.getTicketIssueDate())>0)
		{
			deter = -1;
		}
		return deter;
	}

	public String toString()
	{
		return objectId+"-"+"-"+getLocation()+"-"+getTicketIssueDate()+"-"+getTotalPaid()+"-"+getAccidentIndicator()+"-"+getViolationDescription();
	}
}
