package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorSQ<T extends Comparable <T>> implements Iterator<T> 
{

	private Node<T> siguiente;
	

	
	public IteratorSQ(Node<T> primero)
	{
	
		siguiente = primero;
	}
	/**
	 * 
	 */
	@Override
	public boolean hasNext() 
	{
		return siguiente != null;
	}
	

	@Override
	public T next() 
	{
		if(siguiente == null)
			throw new NoSuchElementException("No hay proximo");
	
		T elemento = siguiente.darElemento();
		siguiente = siguiente.darSiguiente();
		return elemento;	
	}
	
	public T previous() 
	{
		if(siguiente.darAnterior() == null)
			throw new NoSuchElementException("No hay anterior");
	
		T elemento = siguiente.darAnterior().darElemento();
		
		return elemento;	
	}

	/**
	 * Eliminar el ultimo nodo visitado(es decir, el nodo anterior al nodo proximo)
	 */
	public void remove() throws UnsupportedOperationException, IllegalStateException
	{
		
		throw new UnsupportedOperationException("No implementada");
	}

}
